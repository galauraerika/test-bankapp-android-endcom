# test-bankapp-android-endcom

La aplicación de este repositorio se programo bajo las necesidades expresadas en el documento "Test de desarrollo móvil"

Funcionalidad
La aplicación cuenta con dos actividades, la primera se contruye con base a la consulta realizada a los WS presentados en el archivo antes mencionado, la pantalla se forma por diferentes elementos visuales en donde se muestran los datos con los que puede contar un usuario cliente de un banco, mientras que la seguna pantalla muestra un formulario en donde se puede insertar la información necesaria para la adición de una nueva tarjeta.

La estructura del proyecto es la siguiente
- Activities 
- Adapters 
- Models
- Security (Directorio donde se guardan las diferentes url para acceder a los WS)
- WS (Clases asincronas que permiten realizar las consultas a los WS)

Para descargar el proyecto , dirigirse al branch "Master" y descargar el mimos mediante la opcion "Clone", la url ahi brindada debera copiarse y pegarse en un IDE capaz de leer este tipo de proyectos, esta aplicación se creo con Android studio, para realizar la importación del proyecto a esta Herramienta se pueden seguir los siguientes pasos

- Dirigirse al menu "VCS" de Android Studio
- Elegir la opción "Get from Version Control"
- En la ventana desplegada elegir una ruta para el guardado del proyecto y brindar la url antes copiada de git
- El proyecto comenzara a descargarse
